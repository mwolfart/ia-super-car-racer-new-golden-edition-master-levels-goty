import interfaces as controller_template
from itertools import product
from typing import Tuple, List
import numpy
import ast

from random import randint
from math import ceil, exp

class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the car (see below) and compute useful features for selecting an action
        The car has the following sensors:

        self.sensors contains (in order):
            0 track_distance_left: 1-100
            1 track_distance_center: 1-100
            2 track_distance_right: 1-100
            3 on_track: 0 if off track, 1 if on normal track, 2 if on ice
            4 checkpoint_distance: 0-???
            5 car_velocity: 10-200
            6 enemy_distance: -1 or 0-???
            7 enemy_position_angle: -180 to 180
            8 enemy_detected: 0 or 1
            9 checkpoint: 0 or 1
           10 incoming_track: 1 if normal track, 2 if ice track or 0 if car is off track
           11 bomb_distance = -1 or 0-???
           12 bomb_position_angle = -180 to 180
           13 bomb_detected = 0 or 1
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """
        
        # local functions
        def isInInterval(var, min, max):
            return (var > min and var < max)
        
        # constants
        OBJECT_DISTANCE_MAX = 200
        
        # gather data
        dist_left, dist_center, dist_right, on_track, dist_checkpoint = self.sensors[0:5]
        velocity, enemy_distance, enemy_angle, enemy_detected = self.sensors[5:9]
        checkpoint_detected = self.sensors[9]
        bomb_distance, bomb_angle, bomb_detected = self.sensors[11:14]
        
        # renaming variables (readability questions)
        on_ice = (on_track == 2)

        # limit object distances so we have maximum values
        if enemy_distance > OBJECT_DISTANCE_MAX:
            enemy_distance = OBJECT_DISTANCE_MAX
        if bomb_distance > OBJECT_DISTANCE_MAX:
            bomb_distance = OBJECT_DISTANCE_MAX

        # init features
        feat_object_left, feat_object_right, feat_object_front = OBJECT_DISTANCE_MAX, OBJECT_DISTANCE_MAX, OBJECT_DISTANCE_MAX
        feat_track_left, feat_track_right, feat_track_front = 100, 100, 100
        feat_clear_field = 0
        
        # check enemy
        if enemy_detected:
            if isInInterval(enemy_angle, -180, 0):
                feat_object_left = enemy_distance
            elif isInInterval(enemy_angle, 0, 180):
                feat_object_right = enemy_distance
            elif enemy_angle == 0:
                feat_object_front = enemy_distance
                
        # check bomb (higher priority than enemy, so the data is overwritten if both are in the same direction)
        if bomb_detected:
            if isInInterval(bomb_angle, -180, 0):
                feat_object_left = bomb_distance
            elif isInInterval(bomb_angle, 0, 180):
                feat_object_right = bomb_distance
            elif bomb_angle == 0:
                feat_object_front = bomb_distance
        
        # check track boundaries
        if dist_left != -1:
            feat_track_left = dist_left
        if dist_right != -1:
            feat_track_right = dist_right
        if dist_center != -1:
            feat_track_front = dist_center
        
        feat_out_of_track = (on_track == 0)
        feat_velocity = velocity - 10
        
        # save maximum and minimum values (for discretization)
        # if car on ice, features are "taken more seriously"
        if on_ice:
            feat_data_obj_left = (feat_object_left / 2, 0, OBJECT_DISTANCE_MAX)
            feat_data_obj_right = (feat_object_right / 2, 0, OBJECT_DISTANCE_MAX)
            feat_data_obj_front = (feat_object_front / 2, 0, OBJECT_DISTANCE_MAX)
            feat_data_track_left = (feat_track_left / 2, 0, 100)
            feat_data_track_right = (feat_track_right / 2, 0, 100)
            feat_data_track_front = (feat_track_front / 2, 0, 100)
                
            feat_data_out_of_track = (feat_out_of_track, 0, 1)
            feat_data_velocity = (feat_velocity / 2, 0, 190)
        else:
            feat_data_obj_left = (feat_object_left, 0, OBJECT_DISTANCE_MAX)
            feat_data_obj_right = (feat_object_right, 0, OBJECT_DISTANCE_MAX)
            feat_data_obj_front = (feat_object_front, 0, OBJECT_DISTANCE_MAX)
            feat_data_track_left = (feat_track_left, 0, 100)
            feat_data_track_right = (feat_track_right, 0, 100)
            feat_data_track_front = (feat_track_front, 0, 100)
                
            feat_data_out_of_track = (feat_out_of_track, 0, 1)
            feat_data_velocity = (feat_velocity, 0, 190)

        return (feat_data_obj_left, feat_data_obj_right, feat_data_obj_front, feat_data_track_left, feat_data_track_right, feat_data_track_front, feat_data_out_of_track, feat_data_velocity)       

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """
        
        #get discretization levels (if we want to change one, set it in the discretization_levels method)
        discretization_levels = self.discretization_levels()
        factors = [features[i][2] // (discretization_levels[i] - 1) for i in range(len(features))]
        
        #discretize
        max_values = [features[i][2] for i in range(len(features))]
        min_values = [features[i][1] for i in range(len(features))]
        current_values = [features[i][0] for i in range(len(features))]
        discretized_features = [(ceil(current_values[i] / factors[i]) if current_values[i] <= max_values[i] else (discretization_levels[i] - 1)) for i in range(len(features))]
  
        return discretized_features
        
    @staticmethod
    def discretization_levels() -> Tuple:
        return [5,5,5,5,5,5,2,6]

    @staticmethod
    def enumerate_all_possible_states() -> List:
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]


class QTable(controller_template.QTable):
    def __init__(self):
        self.q_table = {}

    def get_q_value(self, key: State, action: int) -> float:
        state_id = key.get_state_id(key.get_current_state()) 
    
        try:
            return self.q_table[(state_id, action)]
        except KeyError:
            self.q_table[(state_id, action)] = 1.0
            return self.q_table[(state_id, action)]

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        state_id = key.get_state_id(key.get_current_state())
        
        self.q_table[(state_id, action)] = new_q_value

    @staticmethod
    def load(path: str) -> "QTable":
        file = open(path, "r")
        new_q_table = QTable()
        new_q_table.q_table = ast.literal_eval(file.read())
        return new_q_table

    def save(self, path: str, *args) -> None:
        file = open(path, "w")
        file.write(str(self.q_table))

        
class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        ALPHA = 0.1
        GAMMA = 0.99
        
        new_q = (1-ALPHA) * self.q_table.get_q_value(old_state, action) + ALPHA * (reward + GAMMA * max([self.q_table.get_q_value(new_state, i) for i in range(1, 6)]))
        self.q_table.set_q_value(old_state, action, new_q)

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_race: bool) -> float:
        
        # Compute features
        new_state_features = new_state.compute_features()
        old_state_features = old_state.compute_features()
        
        # Discretize and unpack
        (feat_obj_left, feat_obj_right, feat_obj_front, feat_track_left, feat_track_right, feat_track_front, feat_out_of_track, feat_velocity) = new_state.discretize_features(new_state_features)
        
        (old_feat_obj_left, old_feat_obj_right, old_feat_obj_front, old_feat_track_left, old_feat_track_right, old_feat_track_front, old_feat_out_of_track, old_feat_velocity) = old_state.discretize_features(old_state_features)
        
        # Init reward
        reward = 0
        
       # Calculate
        if feat_out_of_track:
            reward -= 100
        else:
            reward = feat_obj_left + feat_obj_right + feat_obj_front - abs(feat_track_left - feat_track_right) + feat_track_front
            
        if feat_velocity > old_feat_velocity:
            reward += 100     
     
        if feat_obj_left > old_feat_obj_left: 
            reward += 50  
        
        if feat_obj_right > old_feat_obj_right:  
            reward += 50

        if feat_obj_front > old_feat_obj_front:  
            reward += 50
            
        return reward
    
    def take_action(self, new_state: State, episode_number: int) -> int:
        # Boltzmann exploration
        # Temperature is set according to episode number
        sum_of_q = 0.0
        
        if episode_number == 0 or episode_number % 10 == 0:
            T = 100
        else:
            T = 100 / (episode_number % 10)
        
        for action in range(1, 6):
            sum_of_q += exp(self.q_table.get_q_value(new_state, action) / T)
            
        prob_of_choosing_actions = [0]
            
        for action in range(1, 6):
            try:
                prob_of_choosing_actions.append(exp(self.q_table.get_q_value(new_state, action) / T) / sum_of_q)
            except: 
               
                prob_of_choosing_actions.append(0.999)
        
        i = randint(0, 100)
        sum_of_prob = 0
        
        for action in range(1, 6):
            sum_of_prob += prob_of_choosing_actions[action]
            if float(i)/100.0 < sum_of_prob:
                return action
        
        return 5
        

